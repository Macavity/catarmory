<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */



class Database {
	
	public $dbh;
	public $worlddb;
	public $realmdb;
	public $characterdb;
	public $Armory;

	/**
	 * Set variables and call $this->_db_connect();
	 */
	function __construct($armory) {
		global $db;
		unset($this->dbh);
		$this->Armory = $armory;
		$this->worlddb = $db['worlddb'];
		$this->realmdb = $db['realmdb'];
		$this->characterdb = $db['chardb'];
		$this->_db_connect();
	}

	/**
	 * Connects to database using global settings
	 */
	private function _db_connect() {
		global $db;
		try {
			$this->dbh = new PDO($db['dsn'], $db['user'], $db['password'], array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
			));
			$this->dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		} catch (PDOException $e) {
			$this->Armory->throw_error(510,"Database Connection Error",$e->getMessage());
		}
	}

	/**
	 * Prepares and executes a SQL query
	 * @return PDOStatement statement handler
	 */
	public function query($sql,$parameters) {
		$sth = $this->dbh->prepare($sql);
		try {
			$sth->execute($parameters);
		} catch (PDOException $e) {
			$this->Armory->throw_error(511,"SQL Statement Error",$e->getMessage());
		}
		return $sth;
	}

	/**
	 * Returns latest auto-increment id
	 * @return integer id
	 */
	public function lastId() {
		return $this->dbh->lastInsertId(); 
	}

}
