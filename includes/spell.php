<?php

/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Spell extends Cache {

	public $id;

	protected $db;
	protected $_spell;

	function __construct($db,$id) {
		$this->db = $db;

                // search for cached data. Set variable and stop processing when found.
		if ($this->_spell = $this->get_cache(array('spell',$id),GENERAL_DBC_EXPIRE)) {
			return;
		}

		$get_spell = $this->db->query('
			SELECT ds.`col_0` AS id,ds.`col_1` AS attr0,ds.`col_2` AS attr1,ds.`col_3` AS attr2,ds.`col_4` AS attr3,ds.`col_5` AS attr4,ds.`col_6` AS attr5,ds.`col_7` AS attr6,ds.`col_8` AS attr7,ds.`col_9` AS attr8,ds.`col_10` AS attr9,ds.`col_11` AS attr10,dsct.`col_1` AS cast_time,ddi.`col_1` AS duration,ds.`col_21` AS name,ds.`col_22` AS rank,ds.`col_23` AS description,ds.`col_24` AS tooltip,REPLACE(LOWER(dsi.`col_1`),"interface\\\\icons\\\\","") AS icon,dsao.`col_1` AS proc_stacks, dsao.`col_2` AS proc_chance,dsao.`col_3` AS proc_charges
			FROM `dbc_spell` AS ds
			LEFT JOIN `dbc_spellicon` AS dsi ON (ds.`col_19`=dsi.col_0)
			LEFT JOIN `dbc_spellduration` AS ddi ON (ds.`col_13`=ddi.`col_0`)
                        LEFT JOIN `dbc_spellcasttimes` AS dsct ON (ds.`col_12`=dsct.`col_0`)
			LEFT JOIN `dbc_spellauraoptions` AS dsao ON (ds.`col_32`=dsao.`col_0`)
			WHERE ds.`col_0`=?',
			array($id)
		);

		if ($get_spell->rowCount() == 1) {
			$this->_spell = $get_spell->fetch(PDO::FETCH_ASSOC);
			$this->_fix_icon();
			$this->_get_effects();
			$this->store_cache(array('spell',$id),$this->_spell);

			$this->id = $this->_spell['id'];
		}
	}


	/**
	 * Returns spell informations
	 * @return array spell informations
	 */
	public function get_spell() {
		if (!$this->_spell['id'])
			return;

		// avoid looping here
		$this->_parse();

		return $this->_spell;
	}

	/**
	 * Returns spell name
	 * @return string spell name
	 */
	public function get_name() {
		return $this->_spell['name'];
	}

	/**
	 * Returns spell description
	 * @return string spell description
	 */
	public function get_description() {
		return $this->_spell['description'];
	}

	/**
	 * Returns spell duration
	 * @return float spell duration
	 */
	public function get_duration() {
		return $this->_spell['duration']/1000 . ' sec';
	}

	/**
	 * Returns spell icon
	 * @return string spell icon
	 */
	public function get_icon() {
		return $this->_spell['icon'];
	}

	/**
	 * Returns spell effect basepoints
	 * @return integer spell effect basepoints
	 */
	public function get_effect($idx) {
		return $this->_spell['effects'][$idx];
	}

        /**
         * Returns spell proc chance
         * @return string spell proc chance
         */
        public function get_proc_chance() {
                return $this->_spell['proc_chance'];
        }

        /**
         * Returns spell proc stacks
         * @return string spell proc stacks
         */
        public function get_proc_stacks() {
                return $this->_spell['proc_stacks'];
        }


	/**
	 * Fixes spaces, bouble dots, etc. from icon filename
	 */
	private function _fix_icon() {
		$this->_spell['icon'] = str_replace(' ', '_', $this->_spell['icon']);
		$this->_spell['icon'] = str_replace('.', '', $this->_spell['icon']);
	}

	/**
	 * Get spell effects from dbc_spelleffect
	 */
	private function _get_effects() {
		$get_effects = $this->db->query('
			SELECT dse.`col_1` AS effect,dse.`col_3` AS aura,dse.`col_4` AS aura_period,dse.`col_5` AS basepoints,dse.`col_21` AS trigger_spell,dse.`col_25` as idx,dsr.`col_1` AS min_radius,dsr.`col_3` AS max_radius
			FROM `dbc_spelleffect` AS dse
			LEFT JOIN `dbc_spellradius` AS dsr ON (dse.`col_15`=dsr.`col_0`)
			WHERE dse.`col_24`=?',
			array($this->_spell['id'])
		);

		$this->_spell['effects'] = array();
		foreach ($get_effects->fetchAll(PDO::FETCH_ASSOC) as $e) {
			$this->_spell['effects'][$e['idx']] = $e;
		}
	}

	private function _parse() {

/*
		TODO
		${$42208m1*8*$<mult>} 
		$o1~3
		

*/
		$this->_spell['description'] = preg_replace_callback('/\$(\d+)(\w+)/',array($this, '_replace_with_lookup'), $this->_spell['description']);
		$this->_spell['description'] = preg_replace_callback('/\$(\w+)/',array($this, '_replace_self'), $this->_spell['description']);
		$this->_spell['description'] = preg_replace_callback('/\$([\/\*]\d+);(\d+)(\w+)/',array($this, '_replace_math_with_lookup'), $this->_spell['description']);
		$this->_spell['description'] = preg_replace_callback('/\$([\/\*]\d+);(\w+)/',array($this, '_replace_math_self'), $this->_spell['description']);
		$this->_spell['description'] = preg_replace_callback('/\$\?s(\d+)/',array($this, '_replace_condition'), $this->_spell['description']);
		$this->_spell['description'] = preg_replace('/\$\<(.+?)\>/','\$$1', $this->_spell['description']);
	}

	// example: $6788d or $55672s1
	private function _replace_with_lookup($matches) {
		$spell = new Spell($this->db,$matches[1]);
		return $this->_replace($matches[2],$spell);
	}

	// example: $a1, $t2
	private function _replace_self($matches) {
		return $this->_replace($matches[1],$this);
	}

	// example: $/10;17057s1
	private function _replace_math_with_lookup($matches) {
		$spell = new Spell($this->db,$matches[2]);
		return $this->_math($matches[1],$this->_replace($matches[3],$spell));
	}

	// example: $/10;s2
	private function _replace_math_self($matches) {
		return $this->_math($matches[1],$this->_replace($matches[2],$this));
	}

	// do math operations
	private function _math($operation,$base) {
		$sign = substr($operation,0,1);
		$value = substr($operation,1);
		if ($sign == '/') {
			return $base / $value;
		} else if ($operation == '*') {
			return $base * $value;
		}
	}

	// example: $?s6234
	private function _replace_condition($matches) {
		$spell = new Spell($this->db,$matches[1]);
		return ' [CONDITION '. $spell->get_name(). '] -> ';
	}

	// replace effect values (s1~3,a1~3,t1~t3) and base spell values (h,u)
	private function _replace($what,$spell) {
		if ($what == 'd') {
			return $spell->get_duration();
		} else if ($what == 's1') {
			$effect = $spell->get_effect(0);
			return $effect['basepoints'];
		} else if ($what == 's2') {
			$effect = $spell->get_effect(1);
			return $effect['basepoints'];
		} else if ($what == 's3') {
			$effect = $spell->get_effect(2);
			return $effect['basepoints'];
		} else if ($what == 'a1') {
			$effect = $spell->get_effect(0);
			return $this->_convert_radius($effect['min_radius'],$effect['max_radius']);
		} else if ($what == 'a2') {
			$effect = $spell->get_effect(1);
			return $this->_convert_radius($effect['min_radius'],$effect['max_radius']);
		} else if ($what == 'a3') {
			$effect = $spell->get_effect(2);
			return $this->_convert_radius($effect['min_radius'],$effect['max_radius']);
		} else if ($what == 't1') {
			$effect = $spell->get_effect(0);
			return $effect['aura_period']/1000;
		} else if ($what == 't2') {
			$effect = $spell->get_effect(1);
			return $effect['aura_period']/1000;
		} else if ($what == 't3') {
			$effect = $spell->get_effect(2);
			return $effect['aura_period']/1000;
		} else if ($what == 'h') {
			return $spell->get_proc_chance();
		} else if ($what == 'u') {
			return $spell->get_proc_stacks();
		} else {
			return "\$".$what;
		}
	}

	// radius to yards or yards range
	private function _convert_radius($min,$max) {
		if ($min == $max) {
			if ($max == intval($max)) {
				return intval($max);
			} else {
				return sprint_f("%.2f", $max);
			}
		} else {
			return $min.' ~ '.$max;

		}
	}

}
