/*
 * Copyright (C) 2013 Tomas SoCo Strigner <soco@calista.mine.sk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

 

/**
* Quests class
* @class Quests
* @singleton
*/

Quests = (function(c) {
	var Quests = function() {
		this.init.apply(this, arguments);
	};

	$.extend(true, Quests.prototype, {
		/**
		 * Initialize character Quests
		 * @param {Object} c character object data from server
		 */
		init: function(c) {
			this.character = c;
			this.quests = [];
		},

		/**
		 * Get Quests from server and render them
		 */
		retrieveZone: function(zone) {
			// fetch completed quests by this character in this zone.
			Main.fetch(
				{
					what: 'char',
					action: 'quests',
					guid: this.character.getGuid(),
					zone: zone
				}, function(response) {
					var completedQuests = response.data;

					// Fetch all quests in the zone with their tieles, descriptions, rewards, etc.
					Main.fetch(
						{
							what: 'quests',
							action: 'by_zone',
							zone: zone
						}, function(response) {
							var quests = response.data;
							
							$('#quests_pane').html('');
							
							// use List to render grid of quests, but hide column `Zone` (not necessary - everything in one zone). And add column `Completed` showing yes if quest id presents on completed list
							var list = new List(ListView.templates.quests,quests,{
								hide: ['ZoneOrSort'],
								add: [
								    {
								    	id: 'Completed',
								    	name: 'Completed',
										align: 'center',
										text: function(t,d,s) {
											return search_in_array(d.Id,s.additionalData.quests) ? 'Yes' : '';
										}
								   }
								],
								additionalData: {
									quests: completedQuests
								}
								
							});
							list.render($('#quests_pane'));
						},this
					);
				
				},this
			);
		},
	});
	
	return Quests;
})();
  